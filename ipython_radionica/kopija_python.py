
# coding: utf-8

# In[20]:

#from IPython.core.display import HTML
#css_file = "example.css"
#HTML(open(css_file, 'r').read())


# # IPython notebook radionica
# 
# Dana 25. 10. 2014. na Tehničkom Fakultetu

# Natuknice:
# - jedan
# - dva
# - tri
# 
# Nabrajanje:
# 3. tre
# 1. uno
# 2. due

# $$ x_i = x_{i-1} - \dfrac{f(x_i)}{df(x_i)} $$

# In[17]:

print('probni tekst')


# <img src="xkcd.png" />

# In[18]:

from IPython.core.display import HTML
HTML('''
<video controls>
<source src=http://techslides.com/demos/sample-videos/small.ogv type=video/ogg>
</video>
''')


# In[30]:

from IPython.display import YouTubeVideo as YTV
YTV('GsufPM5TVZk')


# In[48]:

get_ipython().magic(u'matplotlib inline')
import matplotlib.pyplot as plt
import numpy as np
from IPython.html.widgets import interact, interactive, IntSliderWidget, CheckboxWidget, FloatSliderWidget

def moj_graf(A, c, cos0=False):
    x = np.linspace(0, 10, 200)
    y = A * np.cos(x) * np.exp(-c * x)

    plt.figure(figsize = (12, 8))
    plt.plot(x, y)
    plt.xlabel('Vrijeme')
    plt.ylabel('Motivacija')
    plt.grid() # default true
    plt.title('Motivacija na ovoj radionici')
    plt.ylim(-5, 5)
    
    if cos0:
        y0 = np.cos(x)
        plt.plot(x, y0, 'r--')
    
#interact(moj_graf, A = (0, 5, 0.1), c = (0, 1, 0.1))
interact(moj_graf, A = FloatSliderWidget(min=0, max=5, step=1, value=3),          c = FloatSliderWidget(min=0, max=1, step=0.1, value=0))


# In[52]:

from IPython.html import widgets
[w for w in dir(widgets) if w.endswith('Widget')]


# In[56]:

import pandas
pandas.core.format.set_printoptions(notebook_repr_html=True)


# In[61]:

tablica = pandas.read_csv('podaci.csv')

tablica

